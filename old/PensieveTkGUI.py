import os, re, sys
import argparse
import shlex
from tkinter import *
from tkinter import ttk
from tkinter import font
from Pensieve import Pensieve
from PensievePluginLoader import getGUIPlugins, addPluginArgsToArgParse


class PensieveTkGUI():
    def __init__(self):
        # capture and store the Pensieve instance
        self.argParser = argparse.ArgumentParser(description="Pensieve - Magical memory repository")
        self.guiPlugins = getGUIPlugins()
        addPluginArgsToArgParse(self.argParser, self.guiPlugins)
        self.pensieve = Pensieve(self.guiPlugins)

        # define any default values for the GUI instance to be able to access
        self.minWidth = 720
        self.minHeight = 320
        self.defaultMargin = 5
        self.inputPadding = 5
        self.buttonPadding = 4
        self.pensieveIconPath = os.path.join(sys.path[0], "pensieveicon.ico")

        # root pensieve GUI widget setup
        self.pensieveGUI = None
        self.initPensieveGUI()

        # add memory frame setup
        self.addMemoryFrame = None
        self.addMemoryTextbox = None
        self.addMemoryButton = None
        self.addMemoryLabel = None
        self.setupAddMemoryFrame()

        # memory list frame setup
        self.memoryListFrame = None
        self.searchInputStringVar = None
        self.searchInput = None
        self.searchSubmitButton = None
        self.memoryListTree = None
        self.memoryListScrollbar = None
        self.memoryContextMenu = None
        self.setupMemoryListFrame()

        # run code to populate the GUI
        self.parseSearchInputText()

    def centerAndSizeWindow(self, window, width=None, height=None):
        """
        http://stackoverflow.com/q/3352918/1165441
        """
        window.update_idletasks()
        screenWidth = window.winfo_screenwidth()
        screenHeight = window.winfo_screenheight()
        windowWidth = width or window.winfo_width()
        windowHeight = height or window.winfo_height()
        xCoordinate = screenWidth/2 - windowWidth/2
        yCoordinate = screenHeight/2 - windowHeight/2
        windowGeometry = (windowWidth, windowHeight, xCoordinate, yCoordinate)
        window.geometry("%dx%d+%d+%d" % windowGeometry)

    def initPensieveGUI(self):
        self.pensieveGUI = Tk()
        self.pensieveGUI.iconbitmap(self.pensieveIconPath)
        self.pensieveGUI.title("Pensieve - Magical memory repository")
        self.pensieveGUI.minsize(self.minWidth, self.minHeight)
        self.centerAndSizeWindow(self.pensieveGUI, 800, 400)

    def setupAddMemoryFrame(self):
        self.addMemoryFrame = ttk.Frame(self.pensieveGUI)
        self.addMemoryFrame.grid(column=0, row=0, sticky=(N, E, S, W))
        Grid.columnconfigure(self.pensieveGUI, 0, weight=1)
        Grid.rowconfigure(self.pensieveGUI, 0, weight=1)

        self.addMemoryTextbox = Text(self.addMemoryFrame,
                                     padx=self.inputPadding,
                                     pady=self.inputPadding,
                                     font=font.nametofont("TkDefaultFont"),
                                     wrap=WORD)
        self.addMemoryTextbox.grid(column=0, row=0, sticky=(N, E, S, W), columnspan=2)
        Grid.columnconfigure(self.addMemoryFrame, 0, weight=1)
        Grid.rowconfigure(self.addMemoryFrame, 0, weight=1)
        self.addMemoryTextbox.focus()

        self.addMemoryLabel = ttk.Label(self.addMemoryFrame)
        self.addMemoryLabel.grid(column=0, row=1, sticky=(N, S, W))
        self.addMemoryLabel.columnconfigure(0, weight=0)

        addMemoryCommand = lambda: self.addMemory(self.addMemoryTextbox.get("1.0", END))
        self.addMemoryButton = ttk.Button(self.addMemoryFrame,
                                          text="Add Memory",
                                          padding=self.buttonPadding,
                                          command=addMemoryCommand)
        self.addMemoryButton.grid(column=1, row=1, sticky=(N, E, S))
        Grid.columnconfigure(self.addMemoryFrame, 1, weight=5)

        # gives children widgets of addMemoryFrame a 5 pixel margin
        for childWidget in self.addMemoryFrame.winfo_children():
           childWidget.grid(padx=self.defaultMargin, pady=self.defaultMargin)

    def setupMemoryListFrame(self):
        self.memoryListFrame = ttk.Frame(self.pensieveGUI)
        self.memoryListFrame.grid(column=1, row=0, sticky=(N, E, S, W))
        Grid.columnconfigure(self.pensieveGUI, 1, weight=1)
        Grid.rowconfigure(self.pensieveGUI, 0, weight=1)

        self.searchInputStringVar = StringVar(self.memoryListFrame, "-list")
        self.searchInput = ttk.Entry(self.memoryListFrame, textvariable=self.searchInputStringVar)
        self.searchInput.grid(row=0, column=0, sticky=(N, E, W), ipadx=self.inputPadding, ipady=self.inputPadding)
        Grid.columnconfigure(self.memoryListFrame, 0, weight=1)

        self.searchSubmitButton = ttk.Button(self.memoryListFrame,
                                          text="Search Memories",
                                          padding=self.buttonPadding,
                                          command=self.parseSearchInputText)
        self.searchSubmitButton.grid(row=0, column=1, sticky=(N, E), columnspan=2)

        self.memoryListTree = ttk.Treeview(self.memoryListFrame,
                                           columns=("memoryId", "dateTimeAdded", "memory"),
                                           selectmode=BROWSE)
        self.memoryListTree.column("memoryId", width=50, stretch=False)
        self.memoryListTree.heading("memoryId", text="ID", anchor=W)
        self.memoryListTree.column("dateTimeAdded", width=140, stretch=False)
        self.memoryListTree.heading("dateTimeAdded", text="Date & Time Added", anchor=W)
        self.memoryListTree.column("memory", width=400)
        self.memoryListTree.heading("memory", text="Memory", anchor=W)
        self.memoryListTree["show"] = "headings"
        self.memoryListTree.grid(column=0, row=1, sticky=(N, E, S, W), columnspan=2)
        Grid.columnconfigure(self.memoryListFrame, 0, weight=1)
        Grid.rowconfigure(self.memoryListFrame, 1, weight=1)

        self.memoryListScrollbarY = ttk.Scrollbar(self.memoryListFrame, orient=VERTICAL)
        self.memoryListScrollbarY.config(command=self.memoryListTree.yview)
        self.memoryListTree.config(yscroll=self.memoryListScrollbarY.set)
        self.memoryListScrollbarY.grid(column=2, row=1, sticky=(N, E, S))

        self.memoryContextMenu = Menu(self.pensieveGUI, tearoff=0)
        self.memoryContextMenu.add_command(label="View", command=self.viewMemory)

        # gives children widgets of addMemoryFrame a 5 pixel margin
        for childWidget in self.memoryListFrame.winfo_children():
          childWidget.grid(padx=self.defaultMargin, pady=self.defaultMargin)

        # removes the right margin from the tree view and left margin from the scrollbar
        self.memoryListTree.grid(padx=(self.defaultMargin, 0))
        self.memoryListScrollbarY.grid(padx=(0, self.defaultMargin))

        # bind any events to memoryList Frame widgets
        self.searchInput.bind("<Return>", self.parseSearchInputText)
        self.memoryListTree.bind("<Double-Button-1>", self.viewMemory)
        self.memoryListTree.bind("<Button-3>", self.showMemoryContextMenu)

    def addMemory(self, memoryText):
        self.addMemoryTextbox.delete("1.0", END)
        addedMemoryId = self.pensieve.addMemory(memoryText)

        if addedMemoryId is not None:
            addedMemoryMsgTmpl = "Memory with ID of {0} added to your Pensieve."
            self.addMemoryLabel.config(text=addedMemoryMsgTmpl.format(str(addedMemoryId)), foreground="green")
            self.parseSearchInputText()
        else:
            self.addMemoryLabel.config(text="A memory without content isn't a memory", foreground="red")

    def clearMemoryList(self):
        for memoryId in self.memoryListTree.get_children():
            self.memoryListTree.delete(memoryId)

    def replaceMemories(self, replacementMemories):
        self.clearMemoryList()

        if replacementMemories:
            unwantedChars = re.compile("[\n\t\r]")

            for memory in replacementMemories:
                formattedDatetimeStr = memory.DateTimeAdded.strftime("%I:%M:%S %p %m/%d/%Y")
                strippedMemoryText = unwantedChars.sub(" ", memory.MemoryText)
                memoryRowTuple = (memory.MemoryID, formattedDatetimeStr, strippedMemoryText)
                self.memoryListTree.insert("", "end", memory.MemoryID, values=memoryRowTuple)

    def searchMemories(self, searchInputText):
        searchKeywords = searchInputText.split()
        matchingMemories = self.pensieve.searchMemories(searchKeywords)

        if matchingMemories is not None:
            self.replaceMemories(matchingMemories)

    def parseSearchInputText(self, event=None):
        searchInputText = self.searchInputStringVar.get().strip()

        # LOOK AT THIS: http://stackoverflow.com/a/5943381/1165441
        # for possibly getting rid of the print statement (not that it really matters that much)
        try:
            searchInputTextAsArgs = shlex.split(searchInputText)
            argsDict = vars(self.argParser.parse_args(searchInputTextAsArgs))

            # this code is basically duplicated in PensieveCLI (think about possibly refactoring)
            for guiPlugin in self.guiPlugins:
                dest = guiPlugin.cliDict["dest"]
                if argsDict[dest] is not None:
                    guiFnVal = self.pensieve.executePluginFn(dest, argsDict[dest])
                    replacementMemories = guiPlugin.guiOutputFn(guiFnVal)
                    if replacementMemories:
                        self.replaceMemories(replacementMemories)
        except:
            self.searchMemories(searchInputText)

    def viewMemory(self, event=None):
        # select row under mouse
        if event is not None:
            iid = self.memoryListTree.identify_row(event.y)
        else:
            iid = self.memoryListTree.selection()[0]

        if iid:
            memoryToView = self.pensieve.viewMemory(iid)

            # Create a modal dialog
            viewMemoryWindow = Toplevel(self.pensieveGUI)
            viewMemoryWindow.title("View Memory " + iid)
            viewMemoryWindow.transient(self.pensieveGUI)
            viewMemoryWindow.focus_set()
            viewMemoryWindow.grab_set()

            viewMemoryFrame = ttk.Frame(viewMemoryWindow)
            viewMemoryFrame.grid(column=0, row=0, sticky=(N, E, S, W))
            Grid.columnconfigure(viewMemoryWindow, 0, weight=1)
            Grid.rowconfigure(viewMemoryWindow, 0, weight=1)

            memoryIdStr = "Memory ID: " + str(memoryToView.MemoryID)
            memoryIdLabel = ttk.Label(viewMemoryFrame, text=memoryIdStr)
            memoryIdLabel.grid(column=0, row=0, sticky=(N, E, W))

            datetimeAddedStr = "Date & Time Added: " + memoryToView.DateTimeAdded.strftime("%I:%M:%S %p %m/%d/%Y")
            memoryDateTimeLabel = ttk.Label(viewMemoryFrame, text=datetimeAddedStr)
            memoryDateTimeLabel.grid(column=0, row=1, sticky=(N, E, W))

            memoryTextBox = Text(viewMemoryFrame,
                                 padx=self.inputPadding,
                                 pady=self.inputPadding,
                                 background="#fff",
                                 font=font.nametofont("TkDefaultFont"),
                                 wrap=WORD)
            memoryTextBox.grid(column=0, row=2, sticky=(N, E, S, W))
            memoryTextBox.insert(END, memoryToView.MemoryText)
            memoryTextBox.config(state=DISABLED)
            Grid.columnconfigure(viewMemoryFrame, 0, weight=1)
            Grid.rowconfigure(viewMemoryFrame, 2, weight=1)

            memoryTextBoxScrollbarY = ttk.Scrollbar(viewMemoryFrame, orient=VERTICAL)
            memoryTextBoxScrollbarY.config(command=memoryTextBox.yview)
            memoryTextBox.config(yscroll=memoryTextBoxScrollbarY.set)
            memoryTextBoxScrollbarY.grid(column=2, row=2, sticky=(N, E, S))

            # gives children widgets of addMemoryFrame a 5 pixel margin
            for childWidget in viewMemoryFrame.winfo_children():
               childWidget.grid(padx=self.defaultMargin, pady=self.defaultMargin)

            # removes the right margin from the tree view and left margin from the scrollbar
            memoryTextBox.grid(padx=(self.defaultMargin, 0))
            memoryTextBoxScrollbarY.grid(padx=(0, self.defaultMargin))

            # center the window and wait for it to be destroyed
            self.centerAndSizeWindow(viewMemoryWindow, self.minWidth, self.minHeight)
            self.pensieveGUI.wait_window(viewMemoryWindow)
        else:
            # mouse pointer not over item
            # occurs when items do not fill frame
            # no action required
            pass

    def showMemoryContextMenu(self, event=None):
        """
        http://stackoverflow.com/a/25217053/1165441
        :param event:
        :return:
        """

        # select row under mouse
        iid = self.memoryListTree.identify_row(event.y)

        if iid:
            # select the right clicked item in the tree and pop up the context menu
            self.memoryListTree.selection_set(iid)
            self.memoryContextMenu.post(event.x_root, event.y_root)
        else:
            # mouse pointer not over item
            # occurs when items do not fill frame
            # no action required
            pass

    def run(self):
        self.pensieveGUI.mainloop()

def main():
    PensieveTkGUI().run()

if __name__ == "__main__":
    main()