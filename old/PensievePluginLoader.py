import importlib
import os

pluginsDir = "plugins"

def getPluginNames():
    """
    Traverses the pluginsDir and find all file ending in .py and
    not named __init__.py. For each file the extension is ignored
    in order to find the name of the module and that name is then
    appended onto a list which is returned.

    :return: list of python module names
    """
    pluginModules = []
    for p in os.listdir(pluginsDir):
        if p.endswith(".py") and p != "__init__.py":
            pluginModules.append(os.path.splitext(p)[0])
    return pluginModules

def importPlugin(pluginName):
    """
    Given a pluginName the full module path is computed and then
    imported using importlib. Once imported the module is plugin
    is registered and returned if it contains a pensieve function.

    :param pluginName: string name of python file without the .py
    :return: PensievePlugin object if the plugin registers and has
     a pensieve function and if not None.
    """
    fullModulePath = ".".join([pluginsDir, pluginName])
    pluginModule = importlib.import_module(fullModulePath)

    if hasattr(pluginModule, "register"):
        registeredPlugin = pluginModule.register()

        if registeredPlugin.hasPensieveFn:
            return registeredPlugin

    return None

def getPlugins():
    """
    Traverses the output getPluginNames and for each plugin name
    imports a plugin module and appends it to a plugins list
    :return: list of plugin modules
    """
    plugins = []

    for pluginName in getPluginNames():
        plugin = importPlugin(pluginName)
        plugins.append((plugin))

    return plugins

def getCLIPlugins():
    cliPlugins = []

    for plugin in getPlugins():
        if plugin is not None and plugin.supportsCLI:
            cliPlugins.append(plugin)

    return cliPlugins

def addPluginArgsToArgParse(argParser, plugins):
    for plugin in plugins:
        nameOrFlags = plugin.cliDict.pop("nameOrFlags")
        if type(nameOrFlags) is tuple:
            argParser.add_argument(*nameOrFlags, **plugin.cliDict)
        else:
            argParser.add_argument(nameOrFlags, **plugin.cliDict)

def getGUIPlugins():
    guiPlugins = []

    for plugin in getPlugins():
        if plugin is not None and plugin.supportsGUI:
            guiPlugins.append(plugin)

    return guiPlugins