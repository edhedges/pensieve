import sqlite3

class SqlCommander:
    """
    SqlCommander allows for executing sql against a database without having
    to write boilerplate connection / cursor code.
    """

    def __init__(self, database):
        self.database = database

    def Command(self, fn):
        conn = sqlite3.connect(self.database, isolation_level=None, detect_types=sqlite3.PARSE_DECLTYPES)
        cursor = conn.cursor()
        fn(cursor)
        conn.close()
