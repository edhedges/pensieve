from datetime import datetime

class Memory(object):
    """ 
    A magical memory
    """

    def __init__(self, id, dateTimeAdded, memoryText):
        """
        Simple constructor
        """
        self.MemoryID = id
        self.DateTimeAdded = dateTimeAdded
        self.MemoryText = memoryText

    def __str__(self):
        """
        String representation of a memory (used by CLI)
        """
        return "\n" + ("\n".join(["MemoryID:\t" + str(self.MemoryID),
                         "DateTimeAdded:\t" + str(self.DateTimeAdded),
                         "MemoryText:\t" + self.MemoryText,
                         ""]))