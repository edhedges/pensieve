"""
THIS IS GOING TO GET THROWN OUT SOON
"""
import argparse
from Pensieve import Pensieve
from PensievePluginLoader import getCLIPlugins, addPluginArgsToArgParse

addArgKey = "addMemory"
searchArgKey = "searchMemories"
viewArgKey = "viewMemory"

def parseArgs(cliPlugins):
    """
    Uses argparse to set up the arguments to support, parses the arguments provided,
    and returns a dictionary of the arguments if any of them match/have values. Otherwise
    help is printed and None is returned.
    """
    parser = argparse.ArgumentParser(description="Pensieve - Magical memory repository")
    parser.add_argument("-add",
                        dest=addArgKey,
                        nargs="+",
                        type=str,
                        help="Add a memory")
    parser.add_argument("-search",
                        dest=searchArgKey,
                        nargs="+",
                        type=str,
                        help="Search your memories")
    parser.add_argument("-view",
                     dest=viewArgKey,
                     nargs=1,
                     type=int,
                     help="View a memory")

    addPluginArgsToArgParse(parser, cliPlugins)

    argsDict = vars(parser.parse_args())

    if not any(argsDict.values()):
        parser.print_help()
        return None

    return argsDict

def main():
    """
    Main function that is the entry point of the Pensieve CLI
    """
    cliPlugins = getCLIPlugins()
    argsDict = parseArgs(cliPlugins)

    if argsDict is not None:
        pensieve = Pensieve(cliPlugins)

        if argsDict[addArgKey] is not None:
            addedMemoryId = pensieve.addMemory(" ".join(argsDict[addArgKey]))
            addedMemoryMsgTmpl = "Memory with ID of {0} added to your Pensieve."
            print(addedMemoryMsgTmpl.format(str(addedMemoryId)))

        if argsDict[searchArgKey] is not None:
            matchingMemories = pensieve.searchMemories(argsDict[searchArgKey])

            if matchingMemories is not None:
                for memory in matchingMemories:
                    print(memory)
            else:
                print("No matching memories found.")

        if argsDict[viewArgKey] is not None:
            memoryIdToView = argsDict[viewArgKey][0]
            memoryToView = pensieve.viewMemory(memoryIdToView)

            if memoryToView is not None:
                print(memoryToView.MemoryText)
            else:
                memoryNotFoundMsgTmpl = "Memory with ID of {0} not found."
                print(memoryNotFoundMsgTmpl.format(str(memoryIdToView)))

        # this code is basically duplicated in PensieveTkGUI (think about possibly refactoring)
        for cliPlugin in cliPlugins:
            dest = cliPlugin.cliDict["dest"]
            if argsDict[dest] is not None:
                cliFnVal = pensieve.executePluginFn(dest, argsDict[dest])
                cliPlugin.cliOutputFn(cliFnVal)

if __name__ == "__main__":
    main()