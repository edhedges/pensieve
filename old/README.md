CLI python PensieveCLI.py

Pensieve - Magical memory repository

optional arguments:
  -h, --help            show this help message and exit
  -add ADDMEMORY [ADDMEMORY ...]
                        Add a memory
  -search SEARCHMEMORIES [SEARCHMEMORIES ...]
                        Search your memories
  -view VIEWMEMORY      View a memory
  -list [LISTMEMORIES]  Lists the latest N number of memories (N defaults to
                        10)

Version 1.0 TODO:
    - Design and implement editing a memory (think about the if and how it can/should be done with the cli)
        - This will be started from the memory list
    - Finish implementation of plugin architecture (must support cli and future interfaces):
        - Think about how the GUI could best use the plugins dynamically (right now the list plugin is hardcoded into
        the GUIs functionality)
        - Implement a way to support multiples args from plugin func (may be better just to take one arg that's an object)
        - Support a directory with __init__ as module as well as just .py files
    - Design and implement reminder plugin

    -

Notes:
    - For proper centering of the window when opened check this out: http://stackoverflow.com/q/3352918/1165441
    - Got .ico and .png from http://www.deviantart.com/art/Harry-Potter-Pensieve-Icon-Set-260259527