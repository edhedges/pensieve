class PensievePlugin:
    def __init__(self,
                 name,
                 pensieveFn = None,
                 cliDict = None,
                 cliOutputFn = None,
                 guiOutputFn = None,
                 contextMenuFn = None):
        self.name = name
        self.pensieveFn = pensieveFn
        self.cliDict = cliDict
        self.cliOutputFn = cliOutputFn
        self.guiOutputFn = guiOutputFn
        self.contextMenuFn = None

    @property
    def hasPensieveFn(self):
        return self.pensieveFn is not None

    @property
    def supportsCLI(self):
        return self.cliDict is not None and self.cliOutputFn is not None

    @property
    def supportsGUI(self):
        return self.guiOutputFn and (self.cliDict is not None or self.contextMenuFn is not None)