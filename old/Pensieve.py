from datetime import datetime

from SqlCommander import SqlCommander
from Memory import Memory

class Pensieve:
    """
    Magical memory management
    """

    def __init__(self, plugins):
        """
        Runs Pensieve.sql to set up our schema
        """
        self.plugins = plugins
        self.sqlCommander = SqlCommander("Pensieve.db")

        with open("Pensieve.sql") as PensieveSql:
            cursorFn = lambda c: c.executescript(PensieveSql.read())
            self.sqlCommander.Command(cursorFn)

    def addMemory(self, memoryText):
        """
        Adds a memory to the Pensieve

        :param memoryText: string containing a memory
        :return: the integer id of the memory added
        """
        addedMemoryId = None
        strippedMemoryText = memoryText.strip()

        if strippedMemoryText:
            def cursorFn(cursor):
                cursor.execute('''INSERT INTO Memory (Date_Time_Added, Memory_Text)
                    VALUES(?, ?)''', (datetime.now(), memoryText))
                nonlocal addedMemoryId
                addedMemoryId = cursor.lastrowid
            self.sqlCommander.Command(cursorFn)
        
        return addedMemoryId

    def searchMemories(self, keywords):
        """
        Searches the Pensieve for memories matching the given keywords

        :param keywords: list of keywords search for
        :return: list of matching Memory objects
        """
        matchingMemories = None
        keywordsLen = len(keywords)

        if keywordsLen > 0:
            likeStatements = ["Memory_Text LIKE ?"] * keywordsLen
            formattedKeywords = tuple(["%{0}%".format(kw) for kw in keywords]) * 2
            whereClause = "WHERE " + (" OR ".join(likeStatements))
            orderByClause = "ORDER BY " + (" DESC, ".join(likeStatements))
            searchQuery = '''SELECT Memory_ID, Date_Time_Added, Memory_Text
                FROM Memory {0} {1}'''.format(whereClause, orderByClause)
            print(searchQuery)
            def cursorFn(cursor):
                searchResultSet = cursor.execute(searchQuery, formattedKeywords)
                nonlocal matchingMemories
                matchingMemories = [Memory(memRow[0], memRow[1], memRow[2]) for memRow in searchResultSet]
            self.sqlCommander.Command(cursorFn)

        return matchingMemories

    def viewMemory(self, memoryId):
        """
        Queries the Memory table for a memory where the Memory_ID matches the passed in
        memoryId.

        :param memoryId: integer id of a memory
        :return: Memory object
        """
        memory = None

        def cursorFn(cursor):
            nonlocal memory
            memoryResultSet = cursor.execute('''SELECT Memory_ID, Date_Time_Added, Memory_Text
            FROM Memory WHERE Memory_ID = ?''', (memoryId,))
            memoryRow = memoryResultSet.fetchone()
            if memoryRow is not None:
                memory = Memory(memoryRow[0], memoryRow[1], memoryRow[2])
        self.sqlCommander.Command(cursorFn)

        return memory

    def executePluginFn(self, pluginName, arg=None):
        """
        Executes a plugin function by name and passes it an argument
        :param pluginName: name of plugin function
        :param arg: argument the plugin function expects
        :return:
        """
        for p in self.plugins:
            if p.name == pluginName:
                if arg is not None:
                    return p.pensieveFn(self, arg)
                else:
                    return p.pensieveFn(self)
