from PensievePlugin import PensievePlugin
from Memory import Memory

def register():
    pluginName = "listMemories"
    defaultN = 10

    def listMemories(pensieve, numToList=defaultN):
        """
        Lists the latest N number of memories (N defaults to defaultN)

        :param numToList: number of latest memories to list
        :return: latest N number of Memory objects
        """
        listQuery = '''SELECT Memory_ID, Date_Time_Added, Memory_Text
            FROM Memory ORDER BY Memory_ID DESC LIMIT ?'''

        latestNMemories = None
        def cursorFn(cursor):
            listResultSet = cursor.execute(listQuery, (numToList,))
            nonlocal latestNMemories
            latestNMemories = [Memory(memRow[0], memRow[1], memRow[2]) for memRow in listResultSet]
        pensieve.sqlCommander.Command(cursorFn)

        return latestNMemories

    cliDict = dict(nameOrFlags="-list",
                   dest=pluginName,
                   nargs="?",
                   type=int,
                   const=defaultN,
                   default=None,
                   help="Lists the latest N number of memories (N defaults to {0})".format(defaultN))

    def cliOutputFn(latestNMemories):
        if latestNMemories:
            for memory in latestNMemories:
                print(memory)
        else:
            print("No memories in Pensieve.")

    def guiOutputFn(latestNMemories):
        return latestNMemories


    return PensievePlugin(pluginName,
                          pensieveFn=listMemories,
                          cliDict=cliDict,
                          cliOutputFn=cliOutputFn,
                          guiOutputFn=guiOutputFn)

