TODO Features:
    - Delete a memory (in process... missing confirmation dialog)
    - Add Memory_Updated table or simply a Last_Updated column to the Memory table
    - Add some keyboard shortcuts for power users (ME) who would like to not have to use the mouse
        - submit when adding a memory without using mouse (Ctrl + Enter or command + return on mac)
        - navigate memories with keyboard and open memory dialog like this as well
        - same no mouse for memory dialog submission/close and other dialog
    - Plugin system (reminder plugin)
        - Design/implment plugin architecture (look to the python code)
        - Develop reminder plugin and package it by default.
    - Configuration/settings dialog
    - Explore more robust data store options?

DEPENDENCIES:
    npm install bunyan@1.2.3
    sqlite3 on my windows
        npm install sqlite3@3.0.4 --build-from-source --runtime=node-webkit --target_arch=ia32 --target="0.10.5"
    sqlite3 on my mac
        npm install sqlite3@3.0.4 --build-from-source --runtime=node-webkit --target_arch=x64 --target="0.10.5"

    Dependencies were manually installed using the terminal or powershell. "0.10.5" refers to the version of node-webkit.
    This is the latest version supported by node-sqlite3 on both platforms. I had been attempting to build node-sqlite3
    with "0.11.5" (this was the version I had on my mac), but ended up installing a "0.10.5" and that worked.

Windows 7 deployment process:
    - Create pensieve.zip file inside deployment/windows that contains:
        - css
        - js
        - lib
        - node_modules
        - index.html
        - package.json
        - pensieveicon.png
    - Change pensieve.zip to pensieve.nw
    - Open cmd.exe and run: copy /b C:\node-webkit-v0.10.5-win-ia32\nw.exe+pensieve.nw nw.exe
        - has to be named nw.exe for now on Windows: https://github.com/rogerwang/node-webkit/issues/199
    - Delete pensieve.nw
    - Zip up the (icudtl.dat, nw.exe, nw.pak, and other optional files), place in version numbered directory, and deploy!

Mac OS X 10.9.2 deployment process:
    - Create pensieve.zip file inside deployment/mac that contains:
        - css
        - js
        - lib
        - node_modules
        - index.html
        - package.json
        - pensieveicon.png (will need to be made into .icns file)
    - Change pensieve.zip to pensieve.nw
    - Follow this: https://github.com/rogerwang/node-webkit/wiki/How-to-package-and-distribute-your-apps#mac-os-x

Pensieve resources:
    - https://github.com/rogerwang/node-webkit
    - https://github.com/mapbox/node-sqlite3
    - https://github.com/trentm/node-bunyan
    - http://knockoutjs.com/
    - http://getbootstrap.com/

OLD PATH (python34 instead of python27 - Saved this path because building sqlite3 required python27 instead of python34):
C:\Program Files (x86)\NVIDIA Corporation\PhysX\Common;C:\Python34\;C:\Python34\Scripts;C:\Ruby200-x64\bin;C:\Program Files\Common Files\Microsoft Shared\Windows Live;C:\Program Files (x86)\Common Files\Microsoft Shared\Windows Live;C:\Program Files\Broadcom\Broadcom 802.11 Network Adapter\Driver;C:\Program Files (x86)\Intel\iCLS Client\;C:\Program Files\Intel\iCLS Client\;%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem;%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\;C:\Program Files\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files\Intel\Intel(R) Management Engine Components\IPT5;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\IPT;C:\Program Files\WIDCOMM\Bluetooth Software\;C:\Program Files\WIDCOMM\Bluetooth Software\syswow64;C:\Program Files\Java\jre7\bin;C:\Program Files (x86)\Windows Live\Shared;C:\Program Files\Microsoft\Web Platform Installer\;C:\Program Files (x86)\Git\cmd;C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\;C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\;C:\Program Files\Microsoft SQL Server\120\Tools\Binn\;C:\Program Files\Microsoft SQL Server\120\DTS\Binn\;C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\ManagementStudio\;C:\Program Files (x86)\Microsoft SQL Server\120\DTS\Binn\;C:\Program Files\nodejs\;C:\Program Files (x86)\Windows Kits\8.1\Windows Performance Toolkit\;C:\Program Files\Microsoft SQL Server\110\Tools\Binn\;C:\Program Files (x86)\Microsoft SDKs\TypeScript\1.0\