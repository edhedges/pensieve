Running: Double click nw.exe (This is a poorly named executable file because of a bug in a library I used to develop the Pensieve).

Data: Data is stored using sqlite3 in a filed named Pensieve.db. This file lives at %LOCALAPPDATA%/Pensieve.

Issues: If you encounter any issues contact me at eddiehedges@gmail.com with screenshots, descriptions,
and if possible the log file that lives at %LOCALAPPDATA%\Pensieve\Pensieve.log
