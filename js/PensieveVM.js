function PensieveVM(pensieve) {
    var self = this;
    var pensieveMod = {
        pensieve: pensieve,
        openMemoryDialog: function (memory) {
            self.MemoryVM.openMemoryDialog(memory);
        },
        prependNewlyAddedMemory: function () {
            var newlyAddedMemory = null;

            this.pensieve.searchMemories(
                "-list 1",
                function(error, memoryRow) {
                    if(error === null
                        && typeof memoryRow["Memory_ID"] !== "undefined"
                        && typeof memoryRow["Date_Time_Added"] !== "undefined"
                        && typeof memoryRow["Memory_Text"] !== "undefined") {
                        newlyAddedMemory = MemoryListVM.createMemoryObj(memoryRow);
                    }
                    else {
                        global.log.error({ memoryRowError: error });
                    }
                },
                function(error, numMemoryRowsRetrieved) {
                    if(error === null && numMemoryRowsRetrieved > 0 && newlyAddedMemory !== null) {
                        self.MemoryListVM.memories.unshift(newlyAddedMemory);
                    }
                    else {
                        global.log.error({ searchMemoriesCompletionError: error });
                    }
                }
            );
        }
    };

    self.AddMemoryVM = new AddMemoryVM(pensieveMod);
    self.MemoryListVM = new MemoryListVM(pensieveMod);
    self.MemoryVM = new MemoryVM(pensieveMod);
    self.MemoryListVM.searchMemories();
}