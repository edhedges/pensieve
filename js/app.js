var bunyan = require("bunyan");
var nwGui = require("nw.gui");
var appDataPath = nwGui.App.dataPath;
var isWin = /^win/.test(process.platform);
var logFilePath = appDataPath + (isWin ? "\\Pensieve.log" : "/Pensieve.log");

// bunyan logger included for error/fatal logging only
global.log = bunyan.createLogger({
    name: "Pensieve",
    streams: [
        {
            level: "error",
            path: logFilePath
        }
    ]
});

process.on("uncaughtException", function (err) {
    global.log.fatal({ uncaughtException: err });
});

// Added to support native Cut/Copy/Paste
// https://github.com/nwjs/nw.js/wiki/Menu#menucreatemacbuiltinappname
if (!isWin) {
    var guiWindow = nwGui.Window.get();
    var nativeMenuBar = new nwGui.Menu({ type: "menubar" });
    nativeMenuBar.createMacBuiltin("Pensieve");
    guiWindow.menu = nativeMenuBar;
}

var pensieveDbPath = appDataPath + (isWin ? "\\Pensieve.db" : "/Pensieve.db");
var pensieveVM = new PensieveVM(new Pensieve(pensieveDbPath));
var pensieveElem = document.getElementById("Pensieve");

ko.applyBindings(pensieveVM, pensieveElem);

window.onresize = function(event) {
    if(pensieveVM.MemoryVM.showMemoryDialog()) {
        MemoryVM.setTextareaHeight();
    }
};