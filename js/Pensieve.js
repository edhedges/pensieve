var sqlite3 = require("sqlite3").verbose();

function Pensieve(pensieveDbPath) {
    var self = this;
    var db = self.getDatabase(pensieveDbPath);

    self.dbFileName = pensieveDbPath;
    
    db.serialize(function() {
        db.run(
            "CREATE TABLE IF NOT EXISTS Memory ("
            + "Memory_ID INTEGER PRIMARY KEY,"
            + "Date_Time_Added DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'localtime')) NOT NULL,"
            + "Memory_Text VARCHAR)"
        );

        //db.run("ALERT TABLE Memory ADD COLUMN Date_Time_Update DATETIz");
    });
    db.close();

    self.addQuery = "INSERT INTO Memory (Memory_Text) VALUES($memoryText)";
    self.listQuery = "SELECT Memory_ID, Date_Time_Added, Memory_Text"
        + " FROM Memory ORDER BY Memory_ID DESC LIMIT $numToList";
    self.updateQuery = "UPDATE Memory SET Memory_Text = $memoryText WHERE Memory_ID = $memoryId";
    self.deleteQuery = "DELETE FROM Memory WHERE Memory_ID = $memoryId";
}

Pensieve.prototype.getDatabase = function(dbFileName) {
    var self = this;
    var dbFileName = dbFileName || self.dbFileName;
    
    return new sqlite3.Database(dbFileName);
};

Pensieve.prototype.addMemory = function(memoryText, insertMemoryCallback){
    var self = this;
    var trimmedMemoryText = memoryText.trim();
    var addMemoryValueObj = { $memoryText: trimmedMemoryText };
    var db = self.getDatabase();

    db.serialize(function() {
        db.run(self.addQuery, addMemoryValueObj, insertMemoryCallback);
    });
    db.close();
};

Pensieve.prototype.searchMemories = function(userSearchString, rowReturnCallback, completedCallback){
    var self = this;
    var searchQuery;
    var searchQueryNoOrder;
    var orderByClause;
    var keywords = userSearchString.split(" ");
    var db = self.getDatabase();
    var i = 0;

    // handles -list queries (maybe look for -list in any spot of the keywords arr)
    if(keywords.length > 0 && keywords[0] === "-list") {
        var numToList = 5;
        
        if(keywords.length >= 2) {
            numToList = parseInt(keywords[1], 10);

            if(numToList === NaN) {
                numToList = 5;
            }
        }

        db.serialize(function() {
            db.each(self.listQuery, { $numToList: numToList }, rowReturnCallback, completedCallback);
        });
    }
    else if(keywords.length > 0) {
        // extract to function for use in queries like "-list SOME_NUM other text"
        searchQueryNoOrder = "SELECT Memory_ID, Date_Time_Added, Memory_Text FROM Memory WHERE";
        orderByClause = " ORDER BY"

        for(; i < keywords.length; i++) {
            searchQueryNoOrder += " Memory_Text LIKE ?";
            orderByClause += " Memory_Text LIKE ?";

            if(i !== keywords.length - 1) {
                searchQueryNoOrder += " OR ";
                orderByClause += ", ";
            }

            keywords[i] = "%" + keywords[i] + "%";
        }

        searchQuery = searchQueryNoOrder + orderByClause + " DESC";

        db.serialize(function() {
            db.each(searchQuery, keywords.concat(keywords), rowReturnCallback, completedCallback);
        });
    }
    db.close();
};

Pensieve.prototype.updateMemory = function(memoryText, memoryId, updateMemoryCallback) {
    var self = this;
    var updateMemoryObj = { $memoryText: memoryText, $memoryId: memoryId };
    var db = self.getDatabase();

    db.serialize(function() {
        db.run(self.updateQuery, updateMemoryObj, updateMemoryCallback);
    });
    db.close();
}

Pensieve.prototype.deleteMemory = function(memoryId, deleteMemoryCallback) {
    var self = this;
    var memoryToDeleteObj = { $memoryId: memoryId };
    var db = self.getDatabase();

    db.serialize(function() {
        db.run(self.deleteQuery, memoryToDeleteObj, deleteMemoryCallback);
    });
    db.close();
}