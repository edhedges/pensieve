function AddMemoryVM(pensieveMod) {
    var self = this;

    self.pensieveMod = pensieveMod;
    self.addAlertsVM = new AlertVM();
    self.memoryText = ko.observable("");
    self.canAddMemory = ko.computed(canAddMemory);

    function canAddMemory() {
        return self.memoryText().length > 0 && self.memoryText().trim().length > 0;
    }
}

AddMemoryVM.prototype.addMemory = function() {
    var self = this;

    self.pensieveMod.pensieve.addMemory(self.memoryText(), function(error) {
        var stmtObj = this;
        self.addAlertsVM.clearAlerts();
        
        if(error === null && typeof stmtObj.lastID !== "undefined") {
            self.memoryText("");
            self.pensieveMod.prependNewlyAddedMemory();
            self.addAlertsVM.success("Memory with ID of " + stmtObj.lastID + " added.");
            window.setTimeout(function() {
                self.addAlertsVM.clearAlerts();
            }, 10000);
        }
        else {
            global.log.error({ addMemoryError:  error });
            self.addAlertsVM.error("Error adding memory. Please see Pensieve.log for more information.");
        }
    });
};