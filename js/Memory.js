function Memory(id, dateTimeAdded, memoryText) {
    var self = this;
    
    self.MemoryID = id
    self.DateTimeAdded = dateTimeAdded
    self.MemoryText = memoryText
}

Memory.prototype.toString = function() {
    var self = this;
    
    return "\nMemoryID:\t" + self.MemoryID
        + "\nDateTimeAdded:\t" + self.DateTimeAdded
        + "\nMemoryText:\t" + self.MemoryText;
};
