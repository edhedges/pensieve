function MemoryListVM(pensieveMod) {
    var self = this;
    var defaltSearchQuery = "-list";

    self.pensieveMod = pensieveMod;
    self.searchQuery = ko.observable(defaltSearchQuery);
    self.memories = ko.observableArray([]);
    self.showConfirmDeletionDialog = ko.observable(false);
    self.memoryToDeleteIndex = -1;
    self.memoryToDelete = null;

    self.hasSearchQuery = ko.computed(hasSearchQuery);

    function hasSearchQuery() {
        return self.searchQuery().length > 0 && self.searchQuery().trim().length > 0;
    }
}

MemoryListVM.createMemoryObj = function(memoryRow) {
    var dateTimeAdded = new Date(memoryRow["Date_Time_Added"]);
    var formattedDateTimeAdded = Utils.formatDateTime(dateTimeAdded);
    var memoryText = memoryRow["Memory_Text"];
    var memoryTextPreview = memoryText;

    if(memoryText.length > 50) {
        memoryTextPreview = memoryText.substring(0, 49) + "...";
    }

    return {
        memoryId: memoryRow["Memory_ID"],
        dateTimeAdded: dateTimeAdded,
        formattedDateTimeAdded: formattedDateTimeAdded,
        memoryText: memoryText,
        memoryTextPreview: ko.observable(memoryTextPreview),
        editableMemoryText: ko.observable(memoryText),
        hoveredOn: ko.observable(false)
    };
};

MemoryListVM.prototype.searchMemories = function() {
    var self = this;
    var matchingMemories = [];

    self.pensieveMod.pensieve.searchMemories(
        self.searchQuery(),
        function(error, memoryRow) {
            if(error === null
                && typeof memoryRow["Memory_ID"] !== "undefined"
                && typeof memoryRow["Date_Time_Added"] !== "undefined"
                && typeof memoryRow["Memory_Text"] !== "undefined") {
                matchingMemories.push(MemoryListVM.createMemoryObj(memoryRow));
            }
            else {
                global.log.error({ memoryRowError: error });
            }
        },
        function(error, numMemoryRowsRetrieved) {
            if(error === null && numMemoryRowsRetrieved > 0) {
                self.memories(matchingMemories);
            }
            else {
                global.log.error({ searchMemoriesCompletionError: error });
            }
        }
    );
};

MemoryListVM.prototype.toggleHoveredOn = function(memoryIndex) {
    var self = this;
 
    if(self.memories().length > memoryIndex) {
        self.memories()[memoryIndex].hoveredOn(!self.memories()[memoryIndex].hoveredOn());
    }
};

MemoryListVM.prototype.openMemoryDialog = function(memory) {
    var self = this;
    self.pensieveMod.openMemoryDialog(memory);
};

MemoryListVM.prototype.openDeleteMemoryDialog = function(memoryIndex, memory) {
    var self = this;

    self.memoryToDeleteIndex = memoryIndex;
    self.memoryToDelete = memory;
    self.showConfirmDeletionDialog(true);
};

MemoryListVM.prototype.closeDeleteMemoryDialog = function () {
    var self = this;

    self.memoryToDeleteIndex = -1;
    self.memoryToDelete = null;
    self.showConfirmDeletionDialog(false);
};

MemoryListVM.prototype.deleteMemory = function() {
    var self = this;
    console.log("call func that runs sql to delete the memory")
    /*self.pensieveMod.pensieve.deleteMemory(memory.memoryId, function(error) {
        if(error === null && this.changes > 0) {
            // open a confirmation dialog by default (eventually can turn off the extra step in settings)
            // to prevent accidental deletions

            if(self.memories().length > memoryIndex) {

                self.showConfirmDeletionDialog(false);
                self.memories.splice(memoryIndex, 1);
                self.searchMemories();
            }
        }
        else {
            global.log.error({ deleteMemoryError: error });
            self.closeDeleteMemoryDialog(false);
            // SHOW ALERT HERE THAT THE MEMORY FAILED TO DELETE
        }
    });*/
};

MemoryListVM.prototype.confirmDeletion = function() {
    var self = this;

    if(self.memoryToDeleteIndex > -1 && self.memoryToDelete != null) {
        self.deleteMemory();
    }
};