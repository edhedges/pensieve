var Utils = {
    formatDateTime: function(dateTimeObj) {
        var hour = dateTimeObj.getHours();
        var minutes = dateTimeObj.getMinutes();
        var seconds = dateTimeObj.getSeconds();
        var whichMeridiem = "AM";
        var month = dateTimeObj.getMonth() + 1;
        var day = dateTimeObj.getDate();
        var year = dateTimeObj.getFullYear();

        if (hour === 0) {
            hour = 12;
        }
        else if (hour >= 12) {
            hour -= 12;
            whichMeridiem = "PM";
        }

        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        return hour +
            ":" +
            minutes +
            ":" +
            seconds +
            " " +
            whichMeridiem +
            " " +
            month +
            "/" +
            day +
            "/" +
            year;
    },
    getMediaWidthAndHeight: function() {
        // http://stackoverflow.com/a/8876069/1165441
        // Does not include width/height of scrollbars
        return {
            width: document.documentElement.clientWidth,
            height: document.documentElement.clientHeight
        };
    },
    getOuterHeight: function(elem, withMargins) {
        var height = elem.offsetHeight;
        var style = withMargins ? getComputedStyle(elem) : null;

        if(style) {
            height += parseInt(style.marginTop) + parseInt(style.marginBottom);
        }

        return height;
    }
};