function AlertVM() {
    var self = this;

    self.alertClass = {
        success: "alert-success",
        info: "alert-info",
        warning: "alert-warning",
        error: "alert-danger"
    };

    self.alertIconClass = {
        success: "glyphicon glyphicon-ok-sign",
        info: "glyphicon glyphicon-info-sign",
        warning: "glyphicon glyphicon-warning-sign",
        error: "glyphicon glyphicon-exclamation-sign"
    };

    self.alertLinkClass = {
        success: "text-success",
        info: "text-info",
        warning: "text-warning",
        error: "text-danger"
    };

    self.alerts = ko.observableArray([]);

    self.showAlerts = ko.computed(showAlerts);

    function showAlerts () {
        return self.alerts().length > 0;
    }
}

AlertVM.prototype.clearAlerts = function() {
    var self = this;
    self.alerts([]);
};

AlertVM.prototype.dismissAlert = function($index) {
    var self = this;
    self.alerts.splice($index, 1);
}

AlertVM.prototype.createAlertObj = function(alertClass, alertIconClass, alertLinkClass, alertBody) {
    return {
        alertClass: alertClass,
        alertIconClass: alertIconClass,
        alertLinkClass: alertLinkClass,
        alertBody: alertBody
    };
};

AlertVM.prototype.success = function(alertBody) {
    var self = this;

    self.alerts.unshift(
        self.createAlertObj(
            self.alertClass.success,
            self.alertIconClass.success,
            self.alertLinkClass.success,
            alertBody
        )
    );
};

AlertVM.prototype.info = function(alertBody) {
    var self = this;

    self.alerts.unshift(
        self.createAlertObj(
            self.alertClass.info,
            self.alertIconClass.info,
            self.alertLinkClass.info,
            alertBody
        )
    );
};

AlertVM.prototype.warning = function(alertBody) {
    var self = this;

    self.alerts.unshift(
        self.createAlertObj(
            self.alertClass.warning,
            self.alertIconClass.warning,
            self.alertLinkClass.warning,
            alertBody
        )
    );
};

AlertVM.prototype.error = function(alertBody) {
    var self = this;

    self.alerts.unshift(
        self.createAlertObj(
            self.alertClass.error,
            self.alertIconClass.error,
            self.alertLinkClass.error,
            alertBody
        )
    );
};