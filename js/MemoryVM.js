function MemoryVM(pensieveMod) {
    var self = this;

    self.pensieveMod = pensieveMod;
    self.memoryAlertsVM = new AlertVM();
    self.showMemoryDialog = ko.observable(false);
    self.memoryObject = ko.observable({});
    self.memoryTextHasChanged = ko.computed(memoryTextHasChanged);
    self.ignoreSaveChanges = ko.observable(false);
    self.showSaveChangesDialog = ko.observable(false);

    function memoryTextHasChanged() {
        var originalMemoryText = self.memoryObject().memoryText;
        var updatedMemoryText = ko.unwrap(self.memoryObject().editableMemoryText);

        if(typeof originalMemoryText !== "undefined"
            && typeof updatedMemoryText !== "undefined"
            && updatedMemoryText.length > 0
            && updatedMemoryText.trim().length > 0
            && updatedMemoryText !== originalMemoryText
            && updatedMemoryText.trim() !== originalMemoryText) {
            return true;
        }

        return false;
    }
}

MemoryVM.setTextareaHeight = function() {
    var widthAndHeight = Utils.getMediaWidthAndHeight();
    var modalDialogElem = document.getElementById("memoryModalDialog");
    var textareaElem = document.getElementById("editableMemoryTextarea");
    var roomToGrow = widthAndHeight.height - Utils.getOuterHeight(modalDialogElem, true);
    var textAreaHeight = textareaElem.offsetHeight + roomToGrow;
    textareaElem.style.height  = textAreaHeight + "px";
};

MemoryVM.prototype.openMemoryDialog = function(memoryObj) {
    var self = this;
    self.memoryObject(memoryObj);
    self.showMemoryDialog(true);
    MemoryVM.setTextareaHeight();
};

MemoryVM.prototype.closeMemoryDialog = function() {
    var self = this;

    if(self.memoryTextHasChanged()) {
        if(self.ignoreSaveChanges()) {
            self.ignoreSaveChanges(false);
            self.showMemoryDialog(false);
            self.memoryObject().editableMemoryText(self.memoryObject().memoryText);
        }
        else {
            self.showSaveChangesDialog(true);
        }
    }
    else {
        self.showMemoryDialog(false);
        self.memoryObject().editableMemoryText(self.memoryObject().memoryText);
    }
};

MemoryVM.prototype.updateMemory = function() {
    var self = this;
    var memoryText = self.memoryObject().editableMemoryText();
    var memoryId = self.memoryObject().memoryId;

    self.pensieveMod.pensieve.updateMemory(memoryText, memoryId, function(error) {
        var stmtObj = this;
        self.memoryAlertsVM.clearAlerts();

        if(error === null && stmtObj.changes > 0) {
            if(self.memoryObject().memoryText.length > 50) {
                self.memoryObject().memoryTextPreview(memoryText.substring(0, 49) + "...");
            }
            else {
                self.memoryObject().memoryTextPreview(memoryText);
            }
            self.memoryObject().memoryText = memoryText;
            self.showMemoryDialog(false);
            self.memoryAlertsVM.success("Memory with ID of " + memoryId + " updated.");
            window.setTimeout(function() {
                self.memoryAlertsVM.clearAlerts();
            }, 10000)
        }
        else {
            global.log.error({ updateMemoryError: error });
            self.memoryAlertsVM.error(
                "Error updating memory with ID of "
                + memoryId
                + ". Please see Pensieve.log for more information."
            );
        }
    });
};

MemoryVM.prototype.confirmSaveChanges = function() {
    var self = this;
    self.showSaveChangesDialog(false);
    self.updateMemory();
};

MemoryVM.prototype.confirmLoseChanges = function() {
    var self = this;
    self.showSaveChangesDialog(false);
    self.ignoreSaveChanges(true);
    self.closeMemoryDialog();
};